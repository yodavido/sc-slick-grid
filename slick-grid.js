function slickGridSlider ($imgs, $index) {
  var $slider = '<div id="SlickGrid__slider-'+$index+'" class="SlickGrid__slider">';

  for( var i = 0, len = $imgs.length; i < len; i++) {
    $slider = $slider + '<div class="SlickGrid__slider-slide"><img src="'+$imgs[i]+'" alt="" /></div>'
  }

  $slider = $slider + '</div>';
  return $slider;
}

function slickGridCard ($data, $index) {
  var $card = '<div class="SlickGrid__card"> \
    '+ slickGridSlider($data.pImages, $index) +' \
    <div class="SlickGrid__card-details"> \
      <h2>'+ $data.pName +'</h2> \
      <p>'+ $data.pDesc +'</p> \
      <a href="'+ $data.pUrl +'" class="btn"><span>+</span> MORE INFO</a> \
    </div> \
  </div>'

  return $card;
}

function initSlickGrid($collection, $el) {
  var $col = $collection;
  var $container = $el;

  for (var i = 0, len = $col.length; i < len; i++) {
    $container.append(slickGridCard($col[i], i));

    $('#SlickGrid__slider-' + i).slick({
      dots: true,
      prevArrow: '<button class="SlickGrid__button SlickGrid__button-left"><svg class="icon icon-chevron-left"><use xlink:href="#icon-chevron-left"></use></svg></button>',
      nextArrow: '<button class="SlickGrid__button SlickGrid__button-right"><svg class="icon icon-chevron-right"><use xlink:href="#icon-chevron-right"></use></svg></button>',
      customPaging : function(slider, i) {
        return '<a class="SlickGrid__dot" href=""> \
            <svg class="icon icon-radio_button_unchecked"><use xlink:href="#icon-radio_button_unchecked"></use></svg> \
            <svg class="icon icon-brightness_1"><use xlink:href="#icon-brightness_1"></use></svg> \
          </a>';
      }
    });
  }

  $container.append('<div class="SlickGrid__card-spacer"></div>');
  $container.append('<div class="SlickGrid__card-spacer"></div>');

  $('.SlickGrid__button, .SlickGrid__dot').on('click', function(e){
    e.preventDefault();
  });
}

$(document).on('ready', function(){
  initSlickGrid(COLLECTION, $('#sc-slick-grid'));
});
