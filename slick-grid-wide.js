function slickGridWideSlider ($imgs, $index, $sliderId) {
  var $slider = '<div id="SlickGridWide__slider-'+$index+'" class="SlickGridWide__slider">';

  for( var i = 0, len = $imgs.length; i < len; i++) {
    $slider = $slider + '<div class="SlickGridWide__slider-slide"><img src="'+$imgs[i]+'" alt="" /></div>'
  }

  $slider = $slider + '</div>';
  return $slider;
}

function slickGridWideCard ($data, $index, $sliderId) {
  var $card = '<div class="SlickGridWide__card"> \
    '+ slickGridWideSlider($data.pImages, $index, $sliderId) +' \
    <div class="SlickGridWide__card-details"> \
      <h2>'+ $data.pName +'</h2> \
      <p>'+ $data.pDesc +'</p> \
      <a href="'+ $data.pUrl +'" class="btn"><span>+</span> MORE INFO</a> \
    </div> \
  </div>'

  return $card;
}

function initSlickGridWide($collection, $el) {
  var $col = $collection;
  var $container = $el;

  $container.empty();

  for (var i = 0, len = $col.length; i < len; i++) {
    $container.append(slickGridWideCard($col[i], i, $container.attr('id')));

    $('#SlickGridWide__slider-' + i).slick({
      dots: true,
      prevArrow: '<button class="SlickGridWide__button SlickGridWide__button-left"><svg class="icon icon-chevron-left"><use xlink:href="#icon-chevron-left"></use></svg></button>',
      nextArrow: '<button class="SlickGridWide__button SlickGridWide__button-right"><svg class="icon icon-chevron-right"><use xlink:href="#icon-chevron-right"></use></svg></button>',
      customPaging : function(slider, i) {
        return '<a class="SlickGridWide__dot" href="#"> \
            <svg class="icon icon-radio_button_unchecked"><use xlink:href="#icon-radio_button_unchecked"></use></svg> \
            <svg class="icon icon-brightness_1"><use xlink:href="#icon-brightness_1"></use></svg> \
          </a>';
      }
    });
  }

  $('.SlickGridWide__button, .SlickGridWide__dot').on('click', function(e){
    e.preventDefault();
  });
}

$(document).on('ready', function(){
  var collections = [COLLECTION, COLLECTION2];

  initSlickGridWide(COLLECTION, $('#sc-slick-grid-wide'));

  $('.SlickGridWide__tabs a').on('click', function(e){
    e.preventDefault();
    initSlickGridWide(collections[parseInt($(this).data('collection-id'))], $('#sc-slick-grid-wide'))

    $('.SlickGridWide__tab').removeClass('SlickGridWide__tab-active');
    $(this).addClass('SlickGridWide__tab-active');
  });
});
